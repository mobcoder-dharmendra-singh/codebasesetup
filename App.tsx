/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import type { PropsWithChildren } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  NativeModules,
  Platform
} from 'react-native';

import { UiUtils } from './src/app/utils';
import Scaffold from './src/app/components/Scaffold';



function App(): JSX.Element {

  // console.log("=====STTUA BAR HEIGHT===AO====", Platform.OS == 'ios' && UiUtils.getStausBarHeight())

  return (

    <Scaffold contentContainerStyle={[styles.contentContainerStyle]}>
      {/* <View style={styles.sectionContainer}> */}

      {/* <StatusBar translucent={true} backgroundColor={"#000"} /> */}
      {/* <StatusBar
        backgroundColor={"grey"}
        barStyle={"dark-content"}
      /> */}
      <View style={[styles.headContainer]}>
        <View style={styles.headSubConatiner}>
          <Text style={{ color: 'black' }}>{StatusBar.currentHeight}</Text>
        </View>
      </View>
      <Text style={styles.headText}>Pixel Perfect Tets</Text>
      {/* </View> */}

    </Scaffold>

  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    backgroundColor: 'red',
    alignItems: 'center',
    // justifyContent: 'center'
  },
  contentContainerStyle: {
    flex: 1,
    backgroundColor: '#F8F8FB',
  },
  headText: {
    fontSize: UiUtils.getHp(20)
  },
  headContainer: {
    height: UiUtils.getHp(100),
    backgroundColor: 'green',
    width: '100%',
    // width: UiUtils.getWp(380),
    padding: UiUtils.getHp(20)
  },
  headSubConatiner: {
    height: '100%',
    width: '100%',
    backgroundColor: 'yellow',
    justifyContent: 'center',
    alignItems: 'center'
  }

});

export default App;
