import React from "react";
import { View, SafeAreaView, StatusBar } from "react-native";
const AppStatusBar = (props) => {
  const {
    backgroundColor = "#DDE6ED",
    statusBarHeight = StatusBar.currentHeight,
    barStyle = "light-content",
    ...statusBarProps
  } = props;
  return (
    <View
      style={{
        // height: statusBarHeight,
        backgroundColor,
      }}
    >
      <SafeAreaView>
        <StatusBar
          //translucent
          backgroundColor={backgroundColor}
          barStyle={barStyle}
          {...statusBarProps}
        />
      </SafeAreaView>
    </View>
  );
};

export default AppStatusBar;
