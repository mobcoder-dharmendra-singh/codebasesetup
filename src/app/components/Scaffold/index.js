import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Touchable,
} from 'react-native';

import AppStatusBar from '../AppStausBar';
class Scaffold extends Component {
  render() {
    const {
      contentContainerStyle = {},
      statusBarStyle = {},
      scaffoldContainerStyle = {},
      isSafeAreaInset = true,
    } = this.props;
    return (
      // <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={[styles.container, scaffoldContainerStyle]}>
        <AppStatusBar {...statusBarStyle} />
        {/* {(isSafeAreaInset && ( */}
          <SafeAreaView
            style={[styles.contentContainerStyle, contentContainerStyle]}>
            {this.props.children}
          </SafeAreaView>
        {/* // )) || (
        //   <View style={[styles.contentContainerStyle, contentContainerStyle]}>
        //     {this.props.children}
        //   </View>
        // )} */}
      </View>
      // </TouchableWithoutFeedback>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainerStyle: {flex: 1, backgroundColor: 'white'},
});
export default Scaffold;
