//https://docs.adobe.com/content/help/en/target/using/experiences/vec/mobile-viewports.html
import { Dimensions,PixelRatio } from 'react-native'
const { height, width } = Dimensions.get('window')
const CURRENT_RESOLUTION = Math.sqrt(height * height + width * width)

class ViewPorts {
  // IPHONE = {
  //   iPhoneXr: {
  //     width: 414,
  //     height: 896,
  //     //828W*1792H
  //   },
  //   iPhoneXS: {
  //     width: 375,
  //     height: 812,
  //   },
  //   iPhone7:{
  //     width: 375,
  //     height: 667,
  //   }
  // };
  // ANDROID = {
  //   Pixel3: {
  //     width: 392.8,
  //     height: 759.3,
  //   },
  //   GooglePixel2XL: {
  //     width: 412,
  //     height: 823,
  //   },
  // };


  getViewPort() {
    // console.log("====View PORT===SIZE===>>>",height ,width ,"=CURRENT_RESOLUTION=",CURRENT_RESOLUTION)
    return {height,width,CURRENT_RESOLUTION}
    // return this.IPHONE.iPhone7;
  }
}
export default ViewPorts;
