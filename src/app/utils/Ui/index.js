import { StatusBar,NativeModules } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ViewPorts from './ViewPorts';
const { StatusBarManager } = NativeModules;


class UiUtils extends ViewPorts {
  viewPort;
  constructor() {
    super();
    this.viewPort = this.getViewPort();
  }

  getHp(pixels = this.viewPort.height) {
    // console.log("=HEIGHTS PIXELS=>>>",pixels ,"==VIEW POSRT HEIGHT=",this.viewPort.height)
    return hp(((pixels / this.viewPort.height) * 100).toFixed(2));
  }

  getWp(pixels = this.viewPort.width) {
    return wp(((pixels / this.viewPort.width) * 100).toFixed(2));
  }

  // getStausBarHeight(){
  //   StatusBarManager.getHeight((statusBarHeight) => {
  //     return statusBarHeight?.height;
  //   })
  // }
}

export default new UiUtils();
